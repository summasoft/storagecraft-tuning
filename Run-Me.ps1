﻿# !!!!!!!! RUN THIS ONLY ON 2008 R2 AND LATER !!!!!!!!!!!!
$invocation = (Get-Variable MyInvocation -Scope 0).Value
$path = $invocation.InvocationName.Substring(0,$Invocation.InvocationName.LastIndexOf("\"))
$freemem = (wmic OS get TotalVisibleMemorySize /value) | out-string; $freemem = $freemem.split('=')[1];
$freemem = $freemem/1024
$pageSize = 1.5 * $freemem
$mempath = $path+"\Mem-manage.reg"
$lanpath = $path+"\lanman.reg"
$tcppath = $path+"\tcpip.reg"


write-Host "Current physical memory: " $freemem
write-Host "Desired pagefile size: " $pageSize
write-host "Mem reg path: " $mempath

start-process $path\pagefile.bat $pageSize
start-process $path\reg-import.bat $mempath
start-process $path\reg-import.bat $lanpath
start-process $path\reg-import.bat $tcppath