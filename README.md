#Storage Craft Tuning Tool

Runs the suggestions from Storage Craft [KB-241](http://www.storagecraft.com/support/kb/article/241)

###Easy Installation
+ [Download](https://bitbucket.org/summasoft/storagecraft-tuning/downloads/) the folder from this repo
+ Place on BDR in any directory
+ Run Run-Me.ps1

### Advanced Installation
+ [Install Git](https://git-scm.com/download/win)
+ Open CMD
+ run `git clone https://bitbucket.org/summasoft/storagecraft-tuning.git`
+ This will ask for your username and password from this site